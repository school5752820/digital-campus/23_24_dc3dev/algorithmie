# Réseau de reconditionnement

[CA Individuel 2](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=124&reg_typ_id=2&que_str_id=CTSTFR0389&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D119)

Une autre manière très efficace pour réduire l'empreinte carbone de votre entreprise, c'est de maximiser la durée de vie des équipements et d'éviter l'achat de produits neufs.

Un fournisseur de produits reconditionnés vous a contacté pour mettre en place un partenariat : à partir de son catalogue et de ses stocks, combien d'achats neufs allez-vous pouvoir économiser à la planète en achetant chez ce fournisseur ?

## Données

**Entrée**

Ligne 1 : un entier N représentant le nombre de catégories de produits que vous souhaitez acheter

N lignes suivantes : un entier représentant le nombre d'unités dont vous avez besoin, suivi du nom de produit

1 ligne suivante : un entier M représentant le nombre de catégories de produits dans le catalogue du vendeur

M lignes suivantes : un entier représentant le nombre d'unités en stock chez le fournisseur, suivi du nom du produit

**Sortie**

Le nombre maximal de produits que vous pourrez acheter chez le fournisseur reconditionné pour satisfaire vos besoins

## Exemple

Pour l'entrée :

```
3
530 Ordinateur
75 TelephonePortable
12 ServeurST448FX
4
100 TelephonePortable
30 Tablette
150 Ordinateur
82 PackClavierSouris
```

La réponse est :

```
225
```

En effet, vous pourrez acheter 225 produits de votre liste de besoins :

- Vous avez besoin de 530 ordinateurs et le fournisseur en a 150. Vous achetez donc l'intégralité de son stock.
- Vous avez besoin de 75 téléphones et le fournisseur en a 100 en stock. Il n'est pas nécessaire d'acheter tout son stock, vous en commanderez donc 75.
- Vous avez besoin de 12 serveurs ST448FX mais le fournisseur n'en a pas en stock. Avec 150 ordinateurs et 75 téléphones, le total d'achats reconditionné est donc bien de 225.

## Contraintes

- Il y aura au maximum 1000 produits différents parmi vos besoins et parmi le catalogue du fournisseur.
- Le nombre total d'unités dont vous avez besoin pour chaque produit est compris entre 1 et 1 million, tout comme le nombre d'unités en stock.
- Chaque nom de produit apparaîtra au maximum une fois dans votre liste de besoins et dans le catalogue fournisseur.
- Les noms de produits sont des chaînes alphanumériques de taille inférieure à 100.
