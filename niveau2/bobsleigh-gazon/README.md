# Bobsleigh sur gazon

[BPCE - Indiv - 2](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=107&reg_typ_id=2&que_str_id=&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D117#)

La célèbre épreuve de bobsleigh sur gazon approche à grands pas et vous devez aider les organisateurs à placer autant de bobsleigh que possible dans Central Park. Mais il y a un problème : le parc contient des arbres qui risquent de gêner la disposition des bobsleigh.

Vous disposez d'une carte (carrée) du parc sur laquelle les arbres sont représentés par des X et les espaces libres par des . et devez trouver le nombre maximum de bobsleighs que vous pouvez placer dans le parc. Les bobsleighs sont des segments horizontaux de 1x4 cases et ne peuvent pas être placés sur les arbres. Quel est le nombre maximal de bobsleighs que vous pouvez placer dans le parc ? Notez bien que chaque bobsleigh doit être placé horizontalement : les règles du bobsleigh sur gazon sont impénétrables !

## Données

**Entrée**

Ligne 1 : Un entier N (au plus 100) représentant le nombre de lignes et de colonnes du terrain.

N lignes suivantes : Une série de N caractères, des . sur les cases libres, et des X sur les cases contenant un arbre.

**Sortie**

Le nombre maximum de bobsleigh que vous pouvez placer dans le parc.

## Exemple

**Entrée**

```
5
....X
X....
.....
..X..
X...X
```

**Sortie**

```
3
```

En effet, on peut placer trois bobsleighs de la façon suivante (notez bien qu'on ne vous demande pas de renvoyer un tel placement, mais juste le nombre maximum de bobsleighs que l'on peut placer)

```
1111X
 X2222
 3333.
 ..X..
 X...X
```
