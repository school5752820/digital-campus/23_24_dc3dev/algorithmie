import { join } from 'node:path';
import { readdir, readFile } from 'node:fs/promises';
import { test, expect } from 'vitest';
import { TEST_SOLUTIONS } from '#root/env.js';

/**
 * @param {string} directory
 */
export async function solve(directory) {
	const exerciseTests = join(directory, 'tests');
	const testsFiles = await readdir(exerciseTests, {
		recursive: false,
		encoding: 'utf-8',
		withFileTypes: true,
	});
	const inputFiles = testsFiles
		.filter((file) => file.name.startsWith('input'))
		.sort((a, b) => {
			const indexA = a.name.match(/input(?<index>\d+).txt/).groups.index;
			const indexB = b.name.match(/input(?<index>\d+).txt/).groups.index;

			return Number(indexA) - Number(indexB);
		});

	const solverFile = await import(
		join(directory, TEST_SOLUTIONS ? 'solution.js' : 'index.js')
	);
	const solverMethod = solverFile.default;

	for await (let testFile of inputFiles) {
		const testIndex = testFile.name.match(/input(?<index>\d+).txt/).groups
			.index;

		const input = await readFile(join(testFile.path, testFile.name), {
			encoding: 'utf-8',
		});
		const output = await readFile(
			join(testFile.path, `output${testIndex}.txt`),
			{ encoding: 'utf-8' },
		);

		test(`Test n°${testIndex}`, () => {
			expect(solverMethod(input.split('\n'))).toBe(output);
		});
	}
}
