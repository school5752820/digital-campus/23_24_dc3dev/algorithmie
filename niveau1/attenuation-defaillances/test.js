import { solve } from '#utils/solver.js';
import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

await solve(dirname(fileURLToPath(import.meta.url)));
