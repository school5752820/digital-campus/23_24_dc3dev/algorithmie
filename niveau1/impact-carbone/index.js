/**
 * @param {Array<string>} input
 * @return {string}
 */
export default function (input) {
	input.shift();

	let lowestVendor = undefined;
	let lowestImpact = Number.POSITIVE_INFINITY;

	for (let vendor of input) {
		let [name, impact] = vendor.split(' ');
		impact = Number(impact);

		if (impact < lowestImpact) {
			lowestImpact = impact;
			lowestVendor = name;
		}
	}

	return lowestVendor;
}
