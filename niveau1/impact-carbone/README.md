# Energie verte

[CA Individuel 1](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=124&reg_typ_id=2&que_str_id=CTSTFR0389&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D119)

Après avoir brillamment réussi vos premières missions de réduction de l'empreinte environnementale de votre entreprise, votre succès fait parler de lui ! C'est désormais à l'échelle internationale que vous mènerez vos actions de transformation.

Loin de devenir une agence de super-héros, votre impact concret se fera sur le terrain. Après avoir mené un audit des plus gros impacts de l'entreprise, vous réalisez que certains contrats d'énergie alimentant vos bureaux et vos datacenters avec de l'énergie fossile. Il va falloir en changer au plus vite !

Vous avez à votre disposition une liste de fournisseurs d'énergie et leur empreinte carbone associée. À vous de donner le nom du fournisseur ayant l'impact carbone le plus faible.

## Données

**Entrée**

Ligne 1 : un entier N compris entre 1 et 50, le nombre de fournisseurs d'énergie

N lignes suivantes : le nom d'un fournisseur (chaîne de lettres majuscules et minuscules) suivi d'un entier entre 1 et 100 000 représentant son impact carbone.

**Sortie**

Le nom du fournisseur ayant l'impact le plus bas. Il est garanti que cette réponse est unique (pas d'ex-aequo).

## Exemple

Pour l'entrée :

```
5
EcoPowerInc 24
CleanEnergyCo 20
CoalAndGas 450
SolarWattTech 17
EnergyMix 218
```

La réponse est :

```
SolarWattTech
```

En effet, parmi les 5 fournisseurs, c'est celui qui a l'impact carbone le plus bas.
