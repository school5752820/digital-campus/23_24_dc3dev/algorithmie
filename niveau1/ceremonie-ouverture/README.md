# Cérémonie d'ouverture

[BPCE - Indiv - 1](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=107&reg_typ_id=2&que_str_id=&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D117#)

Cette année, vous avez l'insigne honneur d'organiser les Jeux Olympiques. Et qui dit Jeux Olympiques dit cérémonie d'ouverture !

Vous avez opté pour une cérémonie très épurée : chaque délégation se déplace en ligne droite jusqu'à arriver devant le Comité Olympique.

On vous donne la distance initiale (en mètres) de chaque délégation au Comité Olympique ainsi que la vitesse (en mètres par seconde) de chaque délégation : pouvez-vous nous indiquer quelle délégation aura l'honneur de clôturer la cérémonie ? À noter que les délégations ne se gênent pas entre elles et peuvent se doubler sans perdre de temps.

## Données

**Entrée**

Ligne 1 : Un entier N (au plus 196) représentant le nombre de délégations.

N lignes suivantes : une chaîne de caractères puis deux entiers (tous deux compris entre 1 et 200), séparés par des espaces. La chaîne de caractères indique le nom de la délégation, le premier entier indique sa distance au Comité Olympique (en mètres), et le deuxième la vitesse (en mètres par seconde) de la délégation.

**Sortie**

Le nom de la délégation qui clôturera la cérémonie. On vous garantit qu'il y a une seule délégation qui arrive en dernier.

## Exemple

**Entrée**

```
8
Chili 101 2
Estonia 58 1
Romania 102 3
Ecuador 103 4
Madagascar 104 5
Oman 50 1
Nicaragua 51 3
Yemen 53 4
```

**Sortie**

```
Estonia
```

En effet, le Yemen arrive devant le Comité Olympique 13.25 secondes après le départ, suivi du Nicaragua 17 secondes après le départ. Peu après arrivent Madagascar (20.8 secondes après le départ) puis l'Équateur (25.75 secondes après le départ) et enfin la Roumanie (34 secondes après le départ). Bien après, Oman et le Chili atteignent le Comité Olympique au coude à coude (respectivement 50 et 50.5 secondes après le départ), et c'est l'Estonie qui clôt la cérémonie après 58 secondes !
