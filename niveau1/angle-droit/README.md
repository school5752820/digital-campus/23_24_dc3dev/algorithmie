# Angles droits

[La Poste - Finale - 1](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=119&reg_typ_id=2&que_str_id=&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D124#)

Vous avez été chargé·e d'une mission culturelle de la plus haute importance : l'organisation d'une exposition autour du thème de la pop culture au centre Pompidou !

Avant de préparer l'installation des œuvres, vous souhaitez vous assurer que chaque salle du musée est conforme à vos attentes, c'est à dire que les murs de chaque pièce soient bien perpendiculaires afin d'exposer optimalement les œuvres.

Ne disposant pas d'une équerre de maçon, vous décidez d'utiliser votre sagesse mathématique pour déterminer la conformité des angles. Avec simplement 3 mesures de distance (les deux murs et la diagonale de la pièce), vous devriez être capable de déterminer si les murs forment effectivement un angle droit.

![Exemple Image](https://i.ibb.co/GF5zJKQ/example.png)

À partir des 3 mesures données en entrée (dans n'importe quel ordre), déterminez si le triangle engendré comporte un angle droit.

## Données

**Entrée**

Trois lignes : trois nombres entiers représentant les trois mesures de la pièce.

**Sortie**

Si les murs sont perpendiculaires, affichez "OUI", sinon affichez "NON".

## Contraintes

- Les trois mesures seront comprises entre 1 et 1 000 000.
- Les mesures peuvent être données dans le mauvais ordre : la diagonale de la pièce n'est pas nécessairement le troisième nombre, mais elle sera toujours le nombre le plus grand.
- Pour être considéré perpendiculaire, l'angle doit mesurer parfaitement 90 degrés : aucune tolérance n'est acceptée.

## Exemple 1

Pour l'entrée :

```
84
80
118
```

L'angle entre les deux murs est d'environ 92.02°, vous devez donc afficher :

```
NON
```

## Exemple 2

Pour l'entrée :

```
80
116
84
```

Ici, en considérant que la deuxième mesure correspond à la diagonale de la pièce, on retrouve ici bien un angle droit. Vous devrez donc répondre :

```
OUI
```
