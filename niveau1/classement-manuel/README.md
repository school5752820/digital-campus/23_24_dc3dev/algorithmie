# Classement à la main

[MDF - Finale - 1](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?cts_id=104&reg_typ_id=2&que_str_id=&cli_id=45alrk6jpdnaguf3oa3gto2875&rtn_pag=https://www.isograd-testingservices.com//FR/solutions-challenges-de-code?cts_id%3D107#)

Vous ne le savez peut-être pas, mais si on vous donne tous ces exercices à résoudre, c'est avant tout parce qu'on ne sait pas coder. Ainsi, après avoir chronométré à la main le temps de résolution de chaque exercice, et après avoir calculé (sur papier) un hash de chaque code soumis, c'est à la main que l'on élimine les candidats qui ont soumis un code avec le même hash qu'au moins un autre candidat. Enfin, c'est toujours à la main que l'on classe les participants restants par temps de résolution croissant.

Bon, vu la quantité de travail que ça demande, nous allons probablement faire ça avec un seul problème cette année. Et comme on aimerait bien profiter du buffet, disons que seuls les temps de résolution nous intéressent. Vous pouvez nous aider à classer les candidats ?

## Données

**Entrée**

Ligne 1 : un entier N (1 ≤ N ≤ 100), le nombre de soumissions acceptées.

N lignes suivantes : une soumission représentée par un entier et une chaîne de caractères, séparés par des espaces. L'entier représente le temps de résolution de la soumission en minutes, et la chaîne de caractères (en minuscule sans espace) représente le hash de la soumission.

**Sortie**

Les temps de résolution des soumissions non-suspectes triés par temps croissant, un temps de résolution par ligne. Les solutions dont le hash apparaît dans plusieurs soumissions doivent être ignorées. On vous garantit qu'il y a au moins une soumission dont le hash est unique (ne serait-ce que parce qu'on vous fait confiance à vous).

## Exemple

**Entrée**

```
4
2 lessthanfourminutesiswaytoofast
30 shetookhertimebutdidnotcheat
59 waytooslow
3 lessthanfourminutesiswaytoofast
```

**Sortie**

```
30
59
```

Même si deux candidats ont été très rapides (2 et 3 minutes), ils ont soumis le même code, ce qui les disqualifie. La soumission gagnante est donc celle qui a pris 30 minutes, suivie de celle en 59 minutes.
