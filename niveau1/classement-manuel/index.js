/**
 * @param {Array<string>} input
 * @return {string}
 */
export default function (input) {
	input.shift();

	/** @type {{ [key: string]: number }} */
	const submissions = {};

	/** @type Set<string> */
	const blockList = new Set();

	for (let submission of input) {
		const [time, hash] = submission.split(' ');

		if (blockList.has(hash)) {
			continue;
		}

		if (Object.hasOwn(submissions, hash)) {
			delete submissions[hash];
			blockList.add(hash);
			continue;
		}

		submissions[hash] = Number(time);
	}

	return Object.values(submissions)
		.sort((a, b) => a - b)
		.join('\n');
}
